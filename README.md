# Fixedsys True Type Font

Some time ago I needed a true-type-version of the (in)famous "Fixedsys" font.

During my search I stumbled on Markus Gebhard's homepage, where he offered not only the rather cool [ASCII-Art editor JavE](http://www.jave.de) but also a fixedsys.ttf download. Markus found this font on the net, and he has expanded it with some characters that he considered to be very important.

Travis Owens is the author of the first version.

Because I required all characters that were previously available in the .fon variant I completed the missing characters. A Euro symbol has also been added.

I am a novice when it comes to designing this font. If it works for you, enjoy it. If not, I'm sorry. Feel free to enhance it.

## Changelog

### Version 5.00c (2003-02-17)

- Character 0x96 is now assigned to a symbol.

### Version 5.00b (27.01.2003)

- Character 0xE5's (å) ring is not closed any more.
- The null is now correctly positioned and no longer too far to the right.

### Version 5.00a (04.12.2002)

- This is my first more-or-less definitive version.
- The Euro symbol is a bonus.

### Until Version 4

- Markus Gebhard's versions of fixedsys.ttf with some new characters compared to the original version.

### Source

- The first version of fixedsys.ttf originated with Travis Owens. He stopped development and released the font for further improvement.

## Font sizes

- You'll often find that font size 11 gives the best results.
- When using Java try font size 15 (Windows) or 14 (Linux?).

## Legal stuff

This font is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License. as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This font is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

Please understand that I just don't have the time to answer questions about installing and using this font. Read your operating system's manual or search internet for information on this matter.